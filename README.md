# Demo Spring Boot

This project is the backend part of the Demo Application. It takes care of the database connections and population, Rest Api security and endpoint distrubutions

## Prerequirents

- [Eclipse](https://www.eclipse.org/downloads/)
- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](https://maven.apache.org)
- [Postgres](https://www.postgresql.org/download/)
- [PostgreSQL Tools - mac](http://www.psequel.com/)
- [PostgreSQL Tools - windows](https://www.pgadmin.org/)
- [Postman](https://www.getpostman.com/)

## Set up Spring Boot project in Eclipse

Lets launch our Eclipse IDE click File > Import > Maven > existing maven project. Please browes to our cloned projects and select the folder. This will import our Spring Boot app.
And that's it :) To Run our appliction, select the top folder and in the top of the tool bar press the run icon. This will launch our appliction and set up Tomcat server on port 8080.

## Set up DB

Use your prefered Postgres Installer. You will need to create a petstore_db at port 5432
Depeniding on what user under are you going to generate the db, you will have to add your user credentials in our Spring Boot application's application.properties file (src>main>resources)
You don't have to worry about populating the db, Liquibase will take care of adding the schema and populating the database with data on application run.

## Adding users to our application

To add primary users to our application you will have to do a post request with postman. Basiclly the application only supports those users whom verify
with https://graphisoftid-api-test.graphisoft.com/api/Account/PostIsUserWithEmailExists

Configure your request with the followings:

The request url:
```http://localhost/8080/admin/add```

postmethord:
```POST```

constent-type:
```application/json```

requestbody:
```{
		"username" :  "sducsai2@graphisoft.com",
		"password" :  "password",
		"email" :  "sducsai2@graphisoft.com",
		"roles" : [{ "role" : "ADMIN"}]```


This will stiore our users in the database and add roles plus relations to one and other. It will also encrypt out password



