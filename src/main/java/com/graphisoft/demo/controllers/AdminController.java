package com.graphisoft.demo.controllers;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.graphisoft.demo.models.User;
import com.graphisoft.demo.service.api.AdminService;

@CrossOrigin
@RestController
@RequestMapping("/admin/")
public class AdminController {

	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	private AdminService adminService;


	//@PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping("/add")
	public String addUserByAdmin(@RequestBody User user) {

		logger.info("ENTRYPOINT addUserByAdmin() called:");
		return adminService.addUserByAdmin(user);
		
	}

}