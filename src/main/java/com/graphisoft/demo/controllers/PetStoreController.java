package com.graphisoft.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.graphisoft.demo.models.Category;
import com.graphisoft.demo.models.PetStore;
import com.graphisoft.demo.service.api.PetStoreService;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("api/")
public class PetStoreController {

	private static final Logger logger = LoggerFactory.getLogger(PetStoreController.class);

	
	@Autowired
	private PetStoreService petstoreService;

	
	/**
	 * List all petstore item
	 * @return PetStoreItem
	 */
	@RequestMapping(value = "petstore", method = RequestMethod.GET)
	public List<PetStore> list(){
		
		logger.info("ENTRYPOINT list() called:");
		return petstoreService.list();
	}
	
	
	/**
	 * Creates a new petstore entry
	 * @param petStoreItem type of PetStoreItem
	 * @return 
	 */
	@RequestMapping(value = "petstore", method = RequestMethod.POST)
	public void create(@RequestBody PetStore petStoreItem) {
		logger.info("ENTRYPOINT create() called:");
		petstoreService.create(petStoreItem);
	}
	
	/**
	 * Gets specific petstore item by id
	 * @param id type of long
	 * @return PetStoreItem
	 */
	@RequestMapping(value = "petstore/{id}", method = RequestMethod.GET)
	public PetStore get(@PathVariable Integer id) {
		
		logger.info("ENTRYPOINT get() called:");
		return petstoreService.get(id);
	}
	
	/**
	 * Updates specific record by id
	 * @param id
	 * @param petstore
	 * @return PetStore
	 */
	@RequestMapping(value = "petstore/{id}", method = RequestMethod.PUT)
	public PetStore update(@PathVariable Integer id, @RequestBody PetStore petstore) {
		
		logger.info("ENTRYPOINT update() called:");
		return petstoreService.update(id,petstore);
	}

	
	/**
	 * delete record at specific id
	 * @param id
	 * @return List<PetStore>
	 */
	@RequestMapping(value = "petstore/{id}", method = RequestMethod.DELETE)
	public List<PetStore> delete(@PathVariable Integer id) {

		
		logger.info("ENTRYPOINT delete() called:");
		return petstoreService.delete(id);
		
	}
	
	/**
	 * get all categories
	 * @return List<Category>
	 */
	@RequestMapping(value = "categories", method = RequestMethod.GET)
	public List<Category> getCategories(){
		
		logger.info("ENTRYPOINT getCategories() called:");
		return petstoreService.getCategories();
		
	}

}
