package com.graphisoft.demo.services;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.graphisoft.demo.models.User;
import com.graphisoft.demo.repositories.UserRepository;
import com.graphisoft.demo.service.api.AdminService;

@Service
public class AdminServiceImpl implements AdminService{
	
	private static final Logger logger = LoggerFactory.getLogger(PetStoreServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
     
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String addUserByAdmin(User user){
				
		try {
			logger.info("Encrypt new user password");
			String pwd = user.getPassword();
			String encryptPwd = passwordEncoder.encode(pwd);
			user.setPassword(encryptPwd);
			
			logger.info("Accessing userRepository.save()");
			userRepository.save(user);
			
			logger.info("Return feedback message");
			return "user added successfully..."; 
			
		} catch (PersistenceException e){
			logger.error("Error addUserByAdmin() : error user save : {}" + e);
			return "User was not added";
		}
		
	};
	

	
}