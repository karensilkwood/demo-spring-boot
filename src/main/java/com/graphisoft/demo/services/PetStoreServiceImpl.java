package com.graphisoft.demo.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.graphisoft.demo.models.Category;
import com.graphisoft.demo.models.PetStore;
import com.graphisoft.demo.repositories.CategoriesRepository;
import com.graphisoft.demo.repositories.PetStoreRepository;
import com.graphisoft.demo.service.api.PetStoreService;

@Service
public class PetStoreServiceImpl implements PetStoreService{
	
	private static final Logger logger = LoggerFactory.getLogger(PetStoreServiceImpl.class);

	
	@Autowired
	private PetStoreRepository petStoreRepository;
	
	@Autowired
	private CategoriesRepository categoryRepository;
     
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PetStore> list(){
		try {
			logger.info("Accessing petStoreRepository.findAll()");		
			return petStoreRepository.findAll();
		} catch (PersistenceException e){
			logger.error("Error list() : error findAll Method : {}" + e);
			return null;
		}
	};
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void create(PetStore petStoreItem) {
		
		try {
			logger.info("Accessing petStoreRepository.saveAndFlush()");
			petStoreRepository.saveAndFlush(petStoreItem);
		} catch (PersistenceException e){
			logger.error("Error create() : error saveAndFlush Method : {}" + e);
		}
	};
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PetStore get(Integer id){
		try {
			logger.info("Accessing petStoreRepository.getOn()");
			return petStoreRepository.getOne(id);
		} catch (PersistenceException e){
			logger.error("Error get() : error getOne Method : {}" + e);
			return null;
		}
	};
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public PetStore update(Integer id, PetStore petstore) {
		try {
			logger.info("Accessing petStoreRepository.getOne()");
			PetStore exisitingPetStore = petStoreRepository.getOne(id);
			
			logger.info("Copying data from one entry to other");
			exisitingPetStore.setCategory(petstore.getCategory());
			exisitingPetStore.setName(petstore.getName());
			exisitingPetStore.setDescription(petstore.getDescription());
			exisitingPetStore.setAuthor(petstore.getAuthor());
			exisitingPetStore.setPrice(petstore.getPrice());
			exisitingPetStore.setEmail(petstore.getEmail());
			exisitingPetStore.setUploadDate(petstore.getUploadDate());
			
			logger.info("Accessing petStoreRepository.saveAndFlush()");
			return petStoreRepository.saveAndFlush(exisitingPetStore);
			
			
		} catch (PersistenceException e){
			logger.error("Error update()  {}" + e);
			return null;
		}
		
	};
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PetStore> delete(Integer id){

		try {
			logger.info("Accessing petStoreRepository.deleteById()");
			petStoreRepository.deleteById(id);
			return petStoreRepository.findAll();
		} catch (PersistenceException e){
			logger.error("Error delete() : error delete Method : {}" + e);
			return null;
		}		
	};
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Category> getCategories(){

		try {
			logger.info("Accessing categoryRepository.findAll()");
			return categoryRepository.findAll();
		} catch (PersistenceException e){
			logger.error("Error getCategories() : error findAll Method : {}" + e);
			return null;
		}
	};
	
}