package com.graphisoft.demo.service.api;

import com.graphisoft.demo.models.User;

public interface AdminService {

	/**
	 * Add new user
	 * @return String
	 */
	public String addUserByAdmin(User user);
	
}
