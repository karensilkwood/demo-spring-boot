package com.graphisoft.demo.service.api;

import java.util.List;

import com.graphisoft.demo.models.Category;
import com.graphisoft.demo.models.PetStore;
import com.graphisoft.demo.models.User;

public interface PetStoreService {

	/**
	 * List all petstore item
	 * @return PetStoreItem
	 */
	public List<PetStore> list();
	
	/**
	 * Creates a new petstore entry
	 * @param petStoreItem type of PetStoreItem
	 */
	public void create(PetStore petStoreItem);
	
	/**
	 * Gets specific petstore item by id
	 * @param id type of long
	 * @return PetStoreItem
	 */
	public PetStore get(Integer id);
	
	/**
	 * Updates specific record by id
	 * @param id
	 * @param petstore
	 * @return PetStore
	 */
	public PetStore update(Integer id, PetStore petstore);
	
	/**
	 * delete record at specific id
	 * @param id
	 * @return List<PetStore>
	 */
	public List<PetStore> delete(Integer id);
	
	
	/**
	 * get all categories
	 * @return List<Category>
	 */
	public List<Category> getCategories();

}
