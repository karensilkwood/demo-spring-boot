package com.graphisoft.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graphisoft.demo.models.Role;

public interface RoleRepository extends JpaRepository<Role, Integer>{

}