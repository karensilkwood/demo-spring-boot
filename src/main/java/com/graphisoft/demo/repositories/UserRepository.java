package com.graphisoft.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graphisoft.demo.models.User;


public interface UserRepository extends JpaRepository<User, Integer>{

	User findByUsername(String username);

}