package com.graphisoft.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graphisoft.demo.models.Category;

public interface CategoriesRepository extends JpaRepository<Category, Integer> {

}

