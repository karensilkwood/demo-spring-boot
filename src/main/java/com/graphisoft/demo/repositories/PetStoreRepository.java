package com.graphisoft.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.graphisoft.demo.models.PetStore;

public interface PetStoreRepository extends JpaRepository<PetStore, Integer> {

}
