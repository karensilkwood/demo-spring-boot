drop table if exists pet_store;
drop table if exists categories;
drop table if exists user_role;
drop table if exists users;
drop table if exists role;


CREATE TABLE
    pet_store
    (
        id SERIAL PRIMARY KEY,
        category VARCHAR,
        name TEXT,
        description TEXT,
        author VARCHAR,
        price NUMERIC,
        email VARCHAR,
        upload_date TIMESTAMP
    );

CREATE TABLE
    categories
    (
        id SERIAL PRIMARY KEY,
        category VARCHAR
    );

CREATE TABLE 
    users
    (
      user_id SERIAL PRIMARY KEY,  
      username VARCHAR,
      password VARCHAR,
      email VARCHAR
    );

CREATE TABLE
    role(
        role_id SERIAL PRIMARY KEY,
        role VARCHAR
    );

CREATE TABLE 
    user_role(
        user_id INTEGER NOT NULL REFERENCES users(user_id), 
        role_id INTEGER NOT NULL REFERENCES role(role_id),
        PRIMARY KEY (user_id,role_id)
    );


